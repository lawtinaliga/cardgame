# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Card Game
* Version 1.0
- A card game shuffles and distribute card to player and CPU.
- There are 52 cards in a deck for a game.
- Card shuffling and distribution happens before the start of the game.
- Player with the higher card wins.
- A (Ace) is the lowest and K (King) is the highest card.
- The game has twenty six (26) rounds.
- After the rounds, user will be prompted that the game is over and will be asked if he wants to play again.
- If yes, another deck of cards will be shuffled and distributed for the game.
- Else, return to login page,
- The game also has a login page.

### How do I get set up? ###
1. Checkout the repository.
2. Build the APK.
3. Install the APK in a mobile device.
4. Open and start the application.

### Who do I talk to? ###

* Author : Ed Lawrence Tinaliga
* Since : 04-SEP-2017