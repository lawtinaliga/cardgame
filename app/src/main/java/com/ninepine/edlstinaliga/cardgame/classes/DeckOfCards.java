package com.ninepine.edlstinaliga.cardgame.classes;

import com.ninepine.edlstinaliga.cardgame.interfaces.Card;

import java.util.List;

/**
 * Class that represents the deck of cards
 * Contains the 52 cards for the game
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public class DeckOfCards {

    /**
     * The 52 cards in the deck
     */
    private List<Card> cards;

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

}
