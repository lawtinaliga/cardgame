package com.ninepine.edlstinaliga.cardgame.classes;

/**
 * Class that represents a user
 *
 * @author lawtinaliga
 * @since 03-SEP-2017
 */

public class User {

    /**
     * Constructor to create a user
     *
     * @param username - username of the user
     * @param password - password of the user
     */
    public User(String username, String password) {
        this.username = username;
        this.password  = password;
    }

    /**
     * Username of the user
     */
    private String username;
    /**
     * Password of the user
     */

    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
