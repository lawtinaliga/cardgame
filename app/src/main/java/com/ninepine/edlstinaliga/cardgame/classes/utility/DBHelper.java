package com.ninepine.edlstinaliga.cardgame.classes.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ninepine.edlstinaliga.cardgame.classes.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for database connection
 *
 * @author lawtinalgia
 * @since 03-09-2017
 */

public class DBHelper extends SQLiteOpenHelper {

    /**
     * Name of the database
     */
    private static final String DATABASE_NAME = "Users.db";
    /**
     * SQL script to create the USERS table
     */
    private static final String SQL_CREATE_TABLE_USERS = "CREATE TABLE USERS (USERNAME TEXT PRIMARY KEY, PASSWORD TEXT)";
    /**
     * SQL script to drop the USERS table
     */
    private static final String SQL_DROP_TABLE_USERS = "DROP TABLE IF EXISTS USERS";
    /**
     * Table name for the users
     */
    private static final String TABLE_USERS = "USERS";
    /**
     * Username column name
     */
    private static final String COLUMN_USERNAME = "USERNAME";
    /**
     * Password column name
     */
    private static final String COLUMN_PASSWRD = "PASSWORD";

    /**
     * SQL script to select all users
     */
    private static final String SQL_SELECT_ALL_USERS = "SELECT * FROM USERS";

    /**
     * Constructor for the DBHelper
     *
     * @param context - the context
     */
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_USERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_USERS);
        onCreate(sqLiteDatabase);
    }

    /**
     * Insert user in the database
     *
     * @param userName - username of the user
     * @param password - password of the user
     */
    public void insertUser(String userName, String password) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_USERNAME, userName);
        contentValues.put(COLUMN_PASSWRD, password);
        sqLiteDatabase.insert(TABLE_USERS, null, contentValues);
    }

    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor result = sqLiteDatabase.rawQuery(SQL_SELECT_ALL_USERS, null);
        result.moveToFirst();

        while (!result.isAfterLast()) {
            String username = result.getString(result.getColumnIndex(COLUMN_USERNAME));
            String password = result.getString(result.getColumnIndex(COLUMN_PASSWRD));
            userList.add(new User(username, password));
            result.moveToNext();
        }

        if (!result.isClosed()) {
            result.close();
        }

        return userList;
    }

}