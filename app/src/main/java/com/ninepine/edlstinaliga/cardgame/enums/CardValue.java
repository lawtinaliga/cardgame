package com.ninepine.edlstinaliga.cardgame.enums;

/**
 * Enum class for card value
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public enum CardValue {

    ONE(1),

    TWO(2),

    THREE(3),

    FOUR(4),

    FIVE(5),

    SIX(6),

    SEVEN(7),

    EIGHT(8),

    NINE(9),

    TEN(10),

    ELEVEN(11),

    TWELVE(12),

    THIRTEEN(13);

    /**
     * The value of the card
     */
    private int value;

    CardValue(int value) {
        this.value = value;
    }

    /**
     * Get the card value
     *
     * @return value of the card
     */
    public int getValue() {
        return value;
    }

}
