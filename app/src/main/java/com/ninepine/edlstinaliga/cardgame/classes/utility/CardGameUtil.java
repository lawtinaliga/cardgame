package com.ninepine.edlstinaliga.cardgame.classes.utility;

import com.ninepine.edlstinaliga.cardgame.classes.DeckOfCards;
import com.ninepine.edlstinaliga.cardgame.enums.Rank;
import com.ninepine.edlstinaliga.cardgame.enums.Suit;
import com.ninepine.edlstinaliga.cardgame.factory.CardFactory;
import com.ninepine.edlstinaliga.cardgame.interfaces.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility class for the Card Game
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public class CardGameUtil {

    /**
     * Private constructor
     */
    private CardGameUtil() {
    }

    /**
     * Construct the deck of cards
     * Add all cards needed for the game
     *
     * @return the deck of cards for the game
     */
    public static DeckOfCards prepareCards() {
        DeckOfCards deckOfCards = new DeckOfCards();
        List<Card> cardList = new ArrayList<>();
        // Add all card instances
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                Card card = CardFactory.getCard(suit, rank);
                cardList.add(card);
            }
        }
        // Shuffle the cards
        Collections.shuffle(cardList);
        deckOfCards.setCards(cardList);
        return deckOfCards;
    }

    /**
     * Distribute the cards to the player and the AI
     *
     * @param deckOfCards - all the 52 cards in the deck
     * @param playerCards - 26 cards that the player will have
     * @param cpuCards    - 26 cards that the AI will have
     */
    public static void distributeCards(DeckOfCards deckOfCards, List<Card> playerCards, List<Card> cpuCards) {
        List<Card> cardList = deckOfCards.getCards();
        for (int i = 0; i < cardList.size(); i++) {
            Card card = cardList.get(i);
            // Check if even
            if (i % 2 == 0) {
                // Give the card to the player
                playerCards.add(card);
            } else {
                // Give the card to the computer
                cpuCards.add(card);
            }
        }
    }

}
