package com.ninepine.edlstinaliga.cardgame.classes.cards.spade;

import com.ninepine.edlstinaliga.cardgame.enums.CardValue;
import com.ninepine.edlstinaliga.cardgame.enums.Rank;
import com.ninepine.edlstinaliga.cardgame.enums.Suit;
import com.ninepine.edlstinaliga.cardgame.interfaces.Card;
import com.ninpine.edlstinaliga.cardgame.R;

/**
 * Class for the Nine of Spades
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public class NineOfSpades implements Card {

    /**
     * Get the rank of the card
     *
     * @return the rank of the card
     */
    public Rank getRank() {
        return Rank.NINE;
    }

    /**
     * Get the suit of the card
     * Values : Clubs, Spades, Hearts, Diamonds
     *
     * @return the suit of the card
     */
    public Suit getSuit() {
        return Suit.SPADES;
    }

    /**
     * Get the value of the card
     *
     * @return the value of the card
     */
    public CardValue getCardValue() {
        return CardValue.NINE;
    }

    /**
     * Get the image of the card in R
     *
     * @return the image of the card
     */
    public int getCardImage() {
        return R.drawable.s09;
    }

}
