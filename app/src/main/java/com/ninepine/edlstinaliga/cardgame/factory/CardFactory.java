package com.ninepine.edlstinaliga.cardgame.factory;

import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.AceOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.EightOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.FiveOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.FourOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.JackOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.KingOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.NineOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.QueenOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.SevenOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.SixOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.TenOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.ThreeOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.clubs.TwoOfClubs;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.AceOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.EightOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.FiveOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.FourOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.JackOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.KingOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.NineOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.QueenOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.SevenOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.SixOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.TenOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.ThreeOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.diamonds.TwoOfDiamonds;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.AceOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.EightOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.FiveOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.FourOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.JackOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.KingOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.NineOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.QueenOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.SevenOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.SixOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.TenOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.ThreeOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.hearts.TwoOfHearts;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.AceOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.EightOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.FiveOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.FourOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.JackOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.KingOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.NineOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.QueenOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.SevenOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.SixOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.TenOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.ThreeOfSpades;
import com.ninepine.edlstinaliga.cardgame.classes.cards.spade.TwoOfSpades;
import com.ninepine.edlstinaliga.cardgame.enums.Rank;
import com.ninepine.edlstinaliga.cardgame.enums.Suit;
import com.ninepine.edlstinaliga.cardgame.interfaces.Card;

/**
 * Factory class for the card classes
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public class CardFactory {

    /**
     * Private constructor
     */
    private CardFactory() {
    }

    /**
     * Get the card
     *
     * @param suit - suit of the card
     * @param rank - rank of the card
     * @return the card
     */
    public static Card getCard(Suit suit, Rank rank) {
        if (Suit.CLUBS.equals(suit)) {
            return getClubCard(rank);
        } else if (Suit.SPADES.equals(suit)) {
            return getSpadeCard(rank);
        } else if (Suit.HEARTS.equals(suit)) {
            return getHeartCard(rank);
        } else if (Suit.DIAMONDS.equals(suit)) {
            return getDiamondsCard(rank);
        } else {
            return null;
        }
    }

    /**
     * Get the card from the Clubs suit
     *
     * @param rank - rank of the card
     * @return the card
     */
    private static Card getClubCard(Rank rank) {
        if (Rank.ACE.equals(rank)) {
            return new AceOfClubs();
        } else if (Rank.TWO.equals(rank)) {
            return new TwoOfClubs();
        } else if (Rank.THREE.equals(rank)) {
            return new ThreeOfClubs();
        } else if (Rank.FOUR.equals(rank)) {
            return new FourOfClubs();
        } else if (Rank.FIVE.equals(rank)) {
            return new FiveOfClubs();
        } else if (Rank.SIX.equals(rank)) {
            return new SixOfClubs();
        } else if (Rank.SEVEN.equals(rank)) {
            return new SevenOfClubs();
        } else if (Rank.EIGHT.equals(rank)) {
            return new EightOfClubs();
        } else if (Rank.NINE.equals(rank)) {
            return new NineOfClubs();
        } else if (Rank.TEN.equals(rank)) {
            return new TenOfClubs();
        } else if (Rank.JACK.equals(rank)) {
            return new JackOfClubs();
        } else if (Rank.QUEEN.equals(rank)) {
            return new QueenOfClubs();
        } else if (Rank.KING.equals(rank)) {
            return new KingOfClubs();
        } else {
            return null;
        }
    }

    /**
     * Get the card from the Spades suit
     *
     * @param rank - rank of the card
     * @return the card
     */
    private static Card getSpadeCard(Rank rank) {
        if (Rank.ACE.equals(rank)) {
            return new AceOfSpades();
        } else if (Rank.TWO.equals(rank)) {
            return new TwoOfSpades();
        } else if (Rank.THREE.equals(rank)) {
            return new ThreeOfSpades();
        } else if (Rank.FOUR.equals(rank)) {
            return new FourOfSpades();
        } else if (Rank.FIVE.equals(rank)) {
            return new FiveOfSpades();
        } else if (Rank.SIX.equals(rank)) {
            return new SixOfSpades();
        } else if (Rank.SEVEN.equals(rank)) {
            return new SevenOfSpades();
        } else if (Rank.EIGHT.equals(rank)) {
            return new EightOfSpades();
        } else if (Rank.NINE.equals(rank)) {
            return new NineOfSpades();
        } else if (Rank.TEN.equals(rank)) {
            return new TenOfSpades();
        } else if (Rank.JACK.equals(rank)) {
            return new JackOfSpades();
        } else if (Rank.QUEEN.equals(rank)) {
            return new QueenOfSpades();
        } else if (Rank.KING.equals(rank)) {
            return new KingOfSpades();
        } else {
            return null;
        }
    }

    /**
     * Get the card from the Hearts suit
     *
     * @param rank - rank of the card
     * @return the card
     */
    private static Card getHeartCard(Rank rank) {
        if (Rank.ACE.equals(rank)) {
            return new AceOfHearts();
        } else if (Rank.TWO.equals(rank)) {
            return new TwoOfHearts();
        } else if (Rank.THREE.equals(rank)) {
            return new ThreeOfHearts();
        } else if (Rank.FOUR.equals(rank)) {
            return new FourOfHearts();
        } else if (Rank.FIVE.equals(rank)) {
            return new FiveOfHearts();
        } else if (Rank.SIX.equals(rank)) {
            return new SixOfHearts();
        } else if (Rank.SEVEN.equals(rank)) {
            return new SevenOfHearts();
        } else if (Rank.EIGHT.equals(rank)) {
            return new EightOfHearts();
        } else if (Rank.NINE.equals(rank)) {
            return new NineOfHearts();
        } else if (Rank.TEN.equals(rank)) {
            return new TenOfHearts();
        } else if (Rank.JACK.equals(rank)) {
            return new JackOfHearts();
        } else if (Rank.QUEEN.equals(rank)) {
            return new QueenOfHearts();
        } else if (Rank.KING.equals(rank)) {
            return new KingOfHearts();
        } else {
            return null;
        }
    }

    /**
     * Get the card from the Diamonds suit
     *
     * @param rank - rank of the card
     * @return the card
     */
    private static Card getDiamondsCard(Rank rank) {
        if (Rank.ACE.equals(rank)) {
            return new AceOfDiamonds();
        } else if (Rank.TWO.equals(rank)) {
            return new TwoOfDiamonds();
        } else if (Rank.THREE.equals(rank)) {
            return new ThreeOfDiamonds();
        } else if (Rank.FOUR.equals(rank)) {
            return new FourOfDiamonds();
        } else if (Rank.FIVE.equals(rank)) {
            return new FiveOfDiamonds();
        } else if (Rank.SIX.equals(rank)) {
            return new SixOfDiamonds();
        } else if (Rank.SEVEN.equals(rank)) {
            return new SevenOfDiamonds();
        } else if (Rank.EIGHT.equals(rank)) {
            return new EightOfDiamonds();
        } else if (Rank.NINE.equals(rank)) {
            return new NineOfDiamonds();
        } else if (Rank.TEN.equals(rank)) {
            return new TenOfDiamonds();
        } else if (Rank.JACK.equals(rank)) {
            return new JackOfDiamonds();
        } else if (Rank.QUEEN.equals(rank)) {
            return new QueenOfDiamonds();
        } else if (Rank.KING.equals(rank)) {
            return new KingOfDiamonds();
        } else {
            return null;
        }
    }
    
}
