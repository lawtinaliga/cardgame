package com.ninepine.edlstinaliga.cardgame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ninepine.edlstinaliga.cardgame.classes.utility.DBHelper;
import com.ninepine.edlstinaliga.cardgame.classes.User;
import com.ninpine.edlstinaliga.cardgame.R;

/**
 * Activity class for login
 *
 * @author lawtinaliga
 * @since 03-SEP-2017
 */
public class LoginActivity extends Activity {

    /**
     * Login button
     */
    private Button loginButton;
    /**
     * Edit text for the username
     */
    private EditText username;
    /**
     * Edit text for the password
     */
    private EditText password;

    /**
     * Message when login is successful
     */
    private static final String SUCCESS_MESSAGE = "Login Success";
    /**
     * Message when login fails
     */
    private static final String ERROR_MESSAGE = "Invalid username or password";

    /**
     * DBHelper instance use for the database connection
     */
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dbHelper = new DBHelper(this);
        defineComponents();
        addUsers();
    }

    /**
     * Add users in the database
     */
    private void addUsers() {
        dbHelper.insertUser("lawtinaliga", "Baloney1");
        dbHelper.insertUser("melvinaquino", "today123");
        dbHelper.insertUser("janroxas", "9pine");
    }


    /**
     * Define login page components
     */
    private void defineComponents() {
        loginButton = findViewById(R.id.btnLogin);
        username = findViewById(R.id.etUsername);
        password = findViewById(R.id.etPassword);
        setLoginButtonClickListener();
    }

    /**
     * Set on click listener to the login button
     */
    private void setLoginButtonClickListener() {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isUser = false;
                for (User user : dbHelper.getAllUsers()) {
                    if (user.getUsername().equals(username.getText().toString()) &&
                            user.getPassword().equals(password.getText().toString())) {
                        isUser = true;
                        break;
                    }
                }
                if (isUser) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), ERROR_MESSAGE, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
