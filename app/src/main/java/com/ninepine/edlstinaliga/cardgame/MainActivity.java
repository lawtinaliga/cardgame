package com.ninepine.edlstinaliga.cardgame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.ninepine.edlstinaliga.cardgame.classes.utility.CardGameUtil;
import com.ninepine.edlstinaliga.cardgame.classes.DeckOfCards;
import com.ninepine.edlstinaliga.cardgame.interfaces.Card;
import com.ninpine.edlstinaliga.cardgame.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity class for the Card Game
 *
 * @author lawtinaliga
 * @since 03-SEP-2017
 */

public class MainActivity extends Activity {

    /**
     * Number of rounds per deck of cards
     */
    private int numberOfRounds = 1;
    /**
     * List of cards the player have
     */
    private List<Card> playerCards = null;
    /**
     * List of cards the AI have
     */
    private List<Card> cpuCards = null;
    /**
     * Image for the AI's card
     */
    private ImageView cpuCardImage;
    /**
     * Image for the player's card
     */
    private ImageView playerCardImage;
    /**
     * Button to play the game
     */
    private Button playButton;
    /**
     * Message if the player wins
     */
    private static final String MESSAGE_WIN = "You win!";
    /**
     * Message if the player loses
     */
    private static final String MESSAGE_LOSE = "You lose!";
    /**
     * Message if it is a draw match
     */
    private static final String MESSAGE_DRAW = "Draw!";
    /**
     * Message when the game is over
     */
    private static final String MESSAGE_GAME_OVER = "Game Over. Play Again?";
    /**
     * Label text for the OK button
     */
    private static final String LABEL_OK = "OK";
    /**
     * Label text for the Yes button
     */
    private static final String LABEL_YES = "Yes";
    /**
     * Label text for the No button
     */
    private static final String LABEL_NO = "No";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        defineComponents();
        preGame();
    }

    /**
     * Construct the deck of cards, shuffle and distribute cards to players
     */
    private void preGame() {
        DeckOfCards deckOfCards = CardGameUtil.prepareCards();
        playerCards = new ArrayList<>();
        cpuCards = new ArrayList<>();
        CardGameUtil.distributeCards(deckOfCards, playerCards, cpuCards);
        faceDownCards();
    }

    /**
     * Define the card game components
     */
    private void defineComponents() {
        cpuCardImage = findViewById(R.id.cpuCard);
        playerCardImage = findViewById(R.id.playerCard);
        playButton = findViewById(R.id.btnPlay);

        setPlayButtonOnClickListener();
    }

    /**
     * Set on click listener to the play button
     */
    private void setPlayButtonOnClickListener() {
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Reveal cards
                Card playerCard = playerCards.get(numberOfRounds - 1);
                Card cpuCard = cpuCards.get(numberOfRounds - 1);
                revealCards(playerCard, cpuCard);
                compareCards(playerCard, cpuCard);
                numberOfRounds++;
            }
        });
    }

    /**
     * Show the cards in the face up position
     */
    private void revealCards(Card playerCard, Card cpuCard) {
        playerCardImage.setImageResource(playerCard.getCardImage());
        cpuCardImage.setImageResource(cpuCard.getCardImage());
    }

    /**
     * Make the cards in face down position
     */
    private void faceDownCards() {
        playerCardImage.setImageResource(R.drawable.card_back);
        cpuCardImage.setImageResource(R.drawable.card_back);
    }

    /**
     * Compare two cards, the player with the greater card value wins
     * Display popup who won the match
     *
     * @param playerCard - card of the player
     * @param cpuCard    - card of the AI
     */
    private void compareCards(Card playerCard, Card cpuCard) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        String message;
        if (playerCard.getCardValue().getValue() > cpuCard.getCardValue().getValue()) {
            message = MESSAGE_WIN;
        } else if (playerCard.getCardValue().getValue() < cpuCard.getCardValue().getValue()) {
            message = MESSAGE_LOSE;
        } else {
            message = MESSAGE_DRAW;
        }
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(LABEL_OK,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        faceDownCards();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        if (numberOfRounds == 26) {
            createPlayAgainDialog();
        }
    }

    /**
     * Create a dialog to indicate that the game is over
     * Ask the player if he wants to play again
     *
     * If yes, reset the game
     * Else, go to login page
     */
    private void createPlayAgainDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(MESSAGE_GAME_OVER);
        alertDialogBuilder.setPositiveButton(LABEL_YES,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        startActivity(getIntent());
                    }
                });
        alertDialogBuilder.setNegativeButton(LABEL_NO,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}