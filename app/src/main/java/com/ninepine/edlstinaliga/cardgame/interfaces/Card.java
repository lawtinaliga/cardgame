package com.ninepine.edlstinaliga.cardgame.interfaces;

import com.ninepine.edlstinaliga.cardgame.enums.CardValue;
import com.ninepine.edlstinaliga.cardgame.enums.Rank;
import com.ninepine.edlstinaliga.cardgame.enums.Suit;

/**
 * Interface class that represents a card
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public interface Card {

    /**
     * Get the rank of the card
     *
     * @return the rank of the card
     */
    Rank getRank();

    /**
     * Get the suit of the card
     * Values : Clubs, Spades, Hearts, Diamonds
     *
     * @return the suit of the card
     */
    Suit getSuit();

    /**
     * Get the value of the card
     *
     * @return the value of the card
     */
    CardValue getCardValue();

    /**
     * Get the image of the card in R
     *
     * @return the image of the card
     */
    int getCardImage();
}
