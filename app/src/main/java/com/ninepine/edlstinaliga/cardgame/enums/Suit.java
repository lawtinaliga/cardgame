package com.ninepine.edlstinaliga.cardgame.enums;

/**
 * Enum class for the card suits
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public enum Suit {

    CLUBS("Clubs"),

    SPADES("Spades"),

    HEARTS("Hearts"),

    DIAMONDS("Diamonds");

    /**
     * The suit of the card
     */
    private String value;

    Suit(String value) {
        this.value = value;
    }

    /**
     * Get the suit of the card
     *
     * @return suit of the card
     */
    public String getValue() {
        return value;
    }

}
