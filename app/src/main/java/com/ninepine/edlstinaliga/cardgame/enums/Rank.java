package com.ninepine.edlstinaliga.cardgame.enums;

/**
 * Enum class for the rank of the cards
 *
 * @author lawtinaliga
 * @since 02-SEP-2017
 */
public enum Rank {

    ACE("A"),

    TWO("2"),

    THREE("3"),

    FOUR("4"),

    FIVE("5"),

    SIX("6"),

    SEVEN ("7"),

    EIGHT("8"),

    NINE("9"),

    TEN("10"),

    JACK("J"),

    QUEEN("Q"),

    KING("K");

    /**
     * The rank of the card
     */
    private String value;

    Rank(String value) {
        this.value = value;
    }

    /**
     * Get the rank of the card
     *
     * @return rank of the card
     */
    public String getValue() {
        return value;
    }

}
